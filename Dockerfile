FROM ruby:2.3.8
ENV LANG C.UTF-8

# Fuck apt-get. Use powerful apt!
RUN apt update && \
    apt install -y nodejs \
                       vim \
                       mysql-client \
                       --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

RUN apt update && apt install -y curl
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get update && apt install -y nodejs

# Cache bundle install
WORKDIR /tmp
ADD ./Gemfile Gemfile
ADD ./Gemfile.lock Gemfile.lock

# Install Bundler 2
RUN gem install bundler
RUN bundle update --bundler
RUN bundle install

# Install yarn
RUN npm i -g yarn && yarn install

ENV APP_ROOT /rstatsx_root
RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT
COPY . $APP_ROOT

EXPOSE  3000
CMD ["rails", "server", "-b", "0.0.0.0"]

