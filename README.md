# RStatsX [WIP]

Alternative HLStatsX implementation with GPL-free MIT license.

Things you may want to cover:

* Ruby version: 2.6.3

* System dependencies: mysql2... maybe

* Configuration: configure mysql connection

* Database creation: WIP

* Database initialization: WIP

* How to run the test suite: awww... later

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
